package com.example.mypersonalwardrobe.constants

class ImgSourceConst {
    companion object {
        const val GALLERY_RESULT_CODE = 200
        const val CAMERA_RESULT_CODE = 300
    }
}
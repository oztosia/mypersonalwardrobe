package com.example.mypersonalwardrobe.utils

class ObservedPostsSetHolder {
    companion object ObservedPostsSetHolder {
        val postSet = mutableSetOf<String>()
    }
}